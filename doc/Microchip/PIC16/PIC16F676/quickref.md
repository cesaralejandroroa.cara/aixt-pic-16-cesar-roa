# Guia Rápida para PIC16F676
## Referencia del PIC16 utilizado de la marca MICROCHIP
- PIC16F676

**NOTA:** Este microcontrolador PIC16F solo cuenta con salidas digitales, entradas digitales y ADC.

## Nombres de los Pines
Los nombres de los pines se nombran con una letra mayúscula que indica el puerto y un número que indica el pin.


### Nombres originales de los pines para I/O digitales para el PIC16F676
| Puerto | 0 | 1 | 2 | 3 | 4 | 5 | 
|:------:|---|---|---|---|---|---|
| **A**  |RA0|RA1|RA2|RA3|RA4|RA5|
| **C**  |RC0|RC1|RC2|RC3|RC4|RC5|

## Memoria Interna 
Es importante tener en consideración que este microcontrolador dispone de un espacio de memoria extremadamente limitado, concretamente de 64 bits, lo cual se refleja al momento de escribir código.

## Registros propios para I/0 digitales del Microcontrolador
La información detallada sobre los registros específicos de cada microcontrolador se encuentra en su datasheet, donde se puede observar la cantidad de entradas, salidas, convertidores analógico-digitales, pulsos de ancho modulados (PWM) y comunicación serial que el microcontrolador puede admitir. En este caso particular, el microcontrolador en cuestión solo dispone de entradas y salidas digitales, junto con un convertidor analógico a digital. Los registros designados para la configuración de estas entradas y salidas son **TRIS** y **PORT**. Dado que el microcontrolador posee dos puertos que operan como entradas y salidas digitales, los registros se configuran de la siguiente manera: para el puerto **A**, se utilizan **TRISA** y **PORTA**, y para el puerto **C**, se utilizan **TRISC** y **PORTC**.
Cuando configuramos los pines del puerto **A** o **C**, podemos hacerlo en formato binario o hexadecimal. Un valor de **1** se utiliza para definir una entrada digital, mientras que un valor de **0** se emplea para una salida digital. La configuración se muestra de la siguiente manera:

```go
TRISA = 0b111111  // Se declaran todos los pines del puerto A de forma binaria como entradas digitales
PORTA = 0b111111  // Se declaran todos los pines del puerto A de forma binaria como entradas digitales

TRISC = 0b111111  // Se declaran todos los pines del puerto B de forma binaria como entradas digitales
PORTC = 0b111111  // Se declaran todos los pines del puerto B de forma binaria como entradas digitales

TRISA = 0b000000  // Se declaran todos los pines del puerto A de forma binaria como salidas digitales
PORTA = 0b000000  // Se declaran todos los pines del puerto A de forma binaria como salidas digitales

TRISC = 0b000000  // Se declaran todos los pines del puerto B de forma binaria como salidas digitales
PORTC = 0b000000  // Se declaran todos los pines del puerto B de forma binaria como salidas digitales
```
## Registros propios para I/0 analogicas del Microcontrolador
| Puerto | 0 | 1 | 2 | 3 | 4 | 5 | 
|:------:|---|---|---|---|---|---|
| **A**  |AN0|AN1|AN2|-  |AN3|-  |
| **C**  |AN4|AN5|AN6|AN7|-  |-  |

## Registros propios para I/0 analogicas del Microcontrolador