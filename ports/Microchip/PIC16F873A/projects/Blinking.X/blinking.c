// PIC16F873A Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#define _XTAL_FREQ 8000000
#include "../../../PIC16F873A/api/builtin.c"
#include "../../../PIC16F873A/api/machine/pin.c"
#include "../../../PIC16F873A/api/time/sleep_ms.c"
#include "../../api/time/sleep_us.c"

void main(void) {
    ADCON1bits.PCFG = 011; // Se definen todos los pines como I/O digitales
    
    PORTB = 0b00000000;    // Se definen todo el puerto B como apagado
    pin_setup(b6_s,out);
    
    while(1){
        
        pin_high(b6);
        sleep_us(200);
        pin_low(b6);
        sleep_us(200);
    }
}
