#ifndef _AIXT__STRING_H_
#define _AIXT__STRING_H_

#include <string.h>

#define string__assign(DEST, SOURCE)    strcpy(DEST, SOURCE) 

#endif  // _AIXT__STRING_H_